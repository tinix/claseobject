package claseobject;

/**
 *
 * @author tinix
 */
public class ClaseObject {

    public static void main(String[] args) {
        Empleado emp1 = new Empleado("Juan", 10000);
        Empleado emp2 = new Empleado("Juan", 10000);
        
        compararObjetos(emp1 , emp2);
    }
    
    private static void compararObjetos(Empleado emp1, Empleado emp2){
        System.out.println("Contenido Objeto" + emp1);
        
        if (emp1 == emp2) 
            System.out.println("Los Objetos tiene la misma direccion de memoria");
        else
            System.out.println("Los Objetos tienen distancia direccion de memoria");
            
        if (emp1.equals(emp2))
            System.out.println("Los Objetos tine el mismo contenido son iguales");
        else
            System.out.println("Los Objetos No tinene el mismo contenido , No son iguales");
              
        // Reviso el metodo hascode
        if(emp1.hashCode() == emp2.hashCode())
            System.out.println("Los Objetos tiene el mismo codigo hash");
        else
            System.out.println("Los Objetos No tienen el mismo codigo hash");
    }
    
}
